import 'package:flutter/material.dart';
import "./category.dart";
import './test_bloc/test_bloc_screen.dart';

class MyHome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var _scaffoldKey = new GlobalKey<ScaffoldState>();
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      drawer: new _DrawerLeft(),
      appBar: AppBar(
        title: Center(
          child: Text('Home'),
        ),
      ),
      body: Center(
        child: new CateGory(),
      ),
      bottomNavigationBar: new _BottomNavigationBar(scaffoldKey: _scaffoldKey),
    );
  }
}

class _DrawerLeft extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            child: Text('Trà sữa demo'),
            decoration: BoxDecoration(color: Colors.blue),
          ),
          ListTile(
            title: Text('Item 1'),
            onTap: () {
              // Update the state of the app
              // ...
              final snackBar = SnackBar(content: Text('Item 1!'));

// Find the Scaffold in the widget tree and use it to show a SnackBar.
              Scaffold.of(context).showSnackBar(snackBar);
              // Then close the drawer
              Navigator.pop(context);
            },
          ),
          ListTile(
            title: Text('Item 2'),
            onTap: () {
              // Update the state of the app
              // ...
              final snackBar = SnackBar(content: Text('Item 2!'));

// Find the Scaffold in the widget tree and use it to show a SnackBar.
              Scaffold.of(context).showSnackBar(snackBar);
              // Then close the drawer
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}

class _BottomNavigationBar extends StatefulWidget {
  final GlobalKey<ScaffoldState> scaffoldKey;
  _BottomNavigationBar({Key key, this.scaffoldKey}) : super(key: key);

  @override
  _BottomNavigationBarState createState() {
    // TODO: implement createState
    return new _BottomNavigationBarState();
  }
}

class _BottomNavigationBarState extends State<_BottomNavigationBar> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return BottomNavigationBar(
      currentIndex: 0,
      onTap: _onTabTapped,
      items: [
        BottomNavigationBarItem(
          title: new Text('Home'),
          icon: new Icon(Icons.home),
        ),
        BottomNavigationBarItem(
          icon: new Icon(Icons.mail),
          title: new Text('Đơn hàng'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.person),
          title: Text('Điểm thưởng'),
        )
      ],
    );
  }

  _onTabTapped(int index) {
    if (index == 1) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => TestBloc()),
      );
    } else {
      var snackbar = new SnackBar(
        content: new Text('$index'),
      );
      widget.scaffoldKey.currentState.showSnackBar(snackbar);
    }
  }
}
