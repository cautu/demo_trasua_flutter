class UserModel {
   String fullname;
   String phone;
   String email;
   String address;

  UserModel({this.fullname, this.phone, this.email, this.address});

  Map<String, dynamic> get map{
    return {
      'fullname':fullname,
      'phone':phone,
      'email':email,
      'address':address,
    };
  }
}
