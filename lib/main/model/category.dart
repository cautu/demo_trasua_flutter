class CategoryModel {
  final int id;
  final String name;
  final String asset;
  const CategoryModel({this.id, this.name, this.asset});
}
