import './trasua.dart';
import './user.dart';

class BillModel {
  final String codeBill;
  final List<TraSuaModel> listTraSua;
  final UserModel user;

  BillModel({this.codeBill,this.listTraSua, this.user});
}
