class TraSuaModel {
  final String name;
  final int price;
  final String asset;
   int quantity;

  TraSuaModel({this.name, this.price, this.asset});

  Map<String, dynamic> get map{
    return {
      'name':name,
      'price':price,
      'asset':asset,
      'quantity':quantity,
    };
  }

  void setQuantity(int quantityNew) {
    if (quantityNew < 0) {
      quantity = 0;
    }
    quantity = quantityNew;
  }

  int  getQuantity(){
    return quantity;
  }
}

typedef void CartChangedCallback(TraSuaModel traSuaModel, bool inCart);
typedef void DecreseQuatity(TraSuaModel traSuaModel);
typedef void IncreseQuatity(TraSuaModel traSuaModel);
typedef void RemoveItem(TraSuaModel traSuaModel);
