import 'package:bloc/bloc.dart';

import './usersEvent.dart';
import './get_user_state.dart';
import './user.dart';

class GetUsersBloc extends Bloc<UsersEvent, GetUsersState> {
  @override
  // TODO: implement initialState
  GetUsersState get initialState => GetUsersUnInitial();

  @override
  Stream<GetUsersState> mapEventToState(UsersEvent event) async* {
    // TODO: implement mapEventToState
    // yield GetUsersUnInitial();

    yield GetUsersLoading();

    if (event is GetUsersEvent) {
      yield GetUsersSuccess(listUsers());
    }
    // return null;
  }
}
