import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import './get_user_bloc.dart';
import './usersEvent.dart';
import './get_user_state.dart';
import './user.dart';

class TestBloc extends StatefulWidget {
  
  @override
  TestBlocState createState() {
    // TODO: implement createState
    return TestBlocState();
  }
}

class TestBlocState extends State<TestBloc> {
  GetUsersBloc _getUsersBloc;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getUsersBloc = GetUsersBloc();
  }

  @override
  Widget build(BuildContext context) {
    _getUsersBloc.dispatch(GetUsersEvent());

    return BlocProvider(
      builder: (context) => _getUsersBloc,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Test bloc'),
          centerTitle: true,
        ),
        body: BlocBuilder(
          bloc: _getUsersBloc,
          builder: (context, GetUsersState state) {
            if (state is GetUsersUnInitial) {
              return Text('initial');
            } else if (state is GetUsersLoading) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            else if (state is GetUsersSuccess) {
              return _buildListUser(state.users);
            }
            else {
              return Center(
                child: Text('error'),
              );
            }
          },
        ),
      ),
    );
  }
}

Widget _buildListUser(List<User> users) {
  return ListView.separated(
    itemBuilder: (context, index) {
      return ListTile(
        leading: CircleAvatar(
          backgroundImage: NetworkImage(users[index].avatar),
        ),
        title: Text(
          users[index].name,
          style: TextStyle(color: Colors.black, fontSize: 16),
        ),
        subtitle: Text(
          users[index].address,
          style: TextStyle(color: Colors.grey, fontSize: 14),
        ),
      );
    },
    separatorBuilder: (context, index) {
      return Divider(
        height: 1,
      );
    },
    itemCount: users.length,
  );
}
