import 'package:flutter/material.dart';
import './model/category.dart';
import './list_tra_sua.dart';

class CateGory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<CategoryModel> categories = [];
    categories.add(new CategoryModel(
        id: 1, name: 'Hàng bán chạy', asset: 'images/trasua1.jpeg'));
    categories.add(new CategoryModel(
        id: 2, name: 'Trà sữa', asset: 'images/trasua2.jpeg'));
    categories.add(new CategoryModel(
        id: 3, name: 'Món nổi bật', asset: 'images/mon_an.jpeg'));
    categories.add(new CategoryModel(
        id: 4, name: 'Trà sữa cà phê', asset: 'images/caphe.jpeg'));

    // TODO: implement build
    return Container(
      margin: const EdgeInsets.only(left: 10, right: 10),
      child: new ListView(
        children: categories.map((CategoryModel categoryModel) {
          return CategoryItem(categoryModel: categoryModel);
        }).toList(),
      ),
    );
  }
}

class CategoryItem extends StatelessWidget {
  final CategoryModel categoryModel;

  CategoryItem({CategoryModel categoryModel})
      : categoryModel = categoryModel,
        super(key: ObjectKey(categoryModel));

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () => _onTapCategoryItem(categoryModel, context),
      child: Container(
        margin: const EdgeInsets.only(top: 10, bottom: 10),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            ClipRRect(
              borderRadius: new BorderRadius.circular(10.0),
              child: Image(
                image: AssetImage(categoryModel.asset),
                width: 350,
                height: 200,
                fit: BoxFit.cover,
              ),
            ),
            Text(categoryModel.name,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                    fontSize: 20))
          ],
        ),
      ),
    );
  }

  _onTapCategoryItem(categoryModel, context) {
    if (categoryModel.id == 2) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => TraSuaList()),
      );
    } else {
      final snackBar = SnackBar(content: Text(categoryModel.name));
      Scaffold.of(context).showSnackBar(snackBar);
    }
  }
}
