// import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';
import 'model/trasua.dart';
import './dialogInfo.dart';

class Cart extends StatefulWidget {
  List<TraSuaModel> listTraSua;
  Cart({Key key, this.listTraSua}) : super(key: key);

  @override
  _CartState createState() {
    return _CartState();
  }
}

class _CartState extends State<Cart> {
  _handleDecreseQuatity(TraSuaModel traSuaModel) {
    setState(() {
      var quantityOri = traSuaModel.getQuantity();
      if (quantityOri > 0) {
        traSuaModel.setQuantity(quantityOri - 1);
      }
    });
  }

  _handleIncreseQuatity(TraSuaModel traSuaModel) {
    setState(() {
      var quantityOri = traSuaModel.getQuantity();
      traSuaModel.setQuantity(quantityOri + 1);
    });
  }

  _handleRemove(TraSuaModel traSuaModel) {
    setState(() {
      widget.listTraSua.remove(traSuaModel);
    });
  }

  _getTotalMoney() {
    var totalMoney = 0;
    for (var ob in widget.listTraSua) {
      totalMoney += ob.price * ob.quantity;
    }
    return totalMoney;
  }

  _handleCreateOrder() {
    for (var ob in widget.listTraSua) {
      print(ob.map);
    }
    showDialog(
        context: context, builder: (BuildContext context) => CustomDialog(listTraSuaOrder:widget.listTraSua));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Giỏ hàng'),
        ),
        body: Container(
          margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
          child: Column(
            children: <Widget>[
              Expanded(
                child: ListView(
                  children: widget.listTraSua.map((TraSuaModel traSuaModel) {
                    return _CartItem(
                        traSuaModel: traSuaModel,
                        handleDecreseQuatity: _handleDecreseQuatity,
                        handleIncreseQuatity: _handleIncreseQuatity,
                        handleRemove: _handleRemove);
                  }).toList(),
                ),
              ),
              Container(
                decoration: BoxDecoration(color: Colors.green),
                padding: EdgeInsets.all(10.0),
                width: double.infinity,
                child: GestureDetector(
                  onTap: () {
                    _handleCreateOrder();
                  },
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          '${_getTotalMoney()}\$',
                          style: TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold,
                              fontSize: 16),
                        ),
                      ),
                      Text(
                        'Tạo đơn hàng'.toUpperCase(),
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ));
  }
}

class _CartItem extends StatelessWidget {
  final TraSuaModel traSuaModel;
  // final int index;
  final DecreseQuatity handleDecreseQuatity;
  final IncreseQuatity handleIncreseQuatity;
  final RemoveItem handleRemove;

  _CartItem(
      {TraSuaModel traSuaModel,
      this.handleDecreseQuatity,
      this.handleIncreseQuatity,
      this.handleRemove})
      : traSuaModel = traSuaModel,
        super(key: ObjectKey(traSuaModel));

  @override
  Widget build(BuildContext context) {
    // print(traSuaModel.map);
    // TODO: implement build
    return Dismissible(
      key: Key(traSuaModel.name),
      onDismissed: (direction) {
        handleRemove(traSuaModel);
      },
      background: Container(
        child: Icon(
          Icons.delete,
        ),
        color: Colors.red,
      ),
      child: Container(
        margin: EdgeInsets.only(top: 6, bottom: 6),
        decoration: BoxDecoration(
          border: Border(
            // width: 1,
            // color: Colors.grey[300],
            left: BorderSide(
              //                   <--- left side
              color: Colors.blue[100],
              width: 6.0,
            ),
            top: BorderSide(
              //                    <--- top side
              color: Colors.blue[300],
              width: 4.0,
            ),
            right: BorderSide(
              color: Colors.blue[500],
              width: 6.0,
            ),
            bottom: BorderSide(
              color: Colors.blue[800],
              width: 4.0,
            ),
          ),
        ),
        child: Row(
          children: <Widget>[
            Image(
              image: AssetImage(traSuaModel.asset),
              width: 100,
              height: 80,
              fit: BoxFit.cover,
            ),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(
                    left: 20.0, top: 10.0, right: 10.0, bottom: 10.0),
                // color: Colors.grey,
                child: Column(
                  children: <Widget>[
                    Container(
                      alignment: Alignment.centerLeft,
                      child: Text(traSuaModel.name,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              color: Colors.lime)),
                    ),
                    Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
                      child: Text(
                        '${traSuaModel.price}\$',
                        style: TextStyle(
                            color: Colors.pink, fontWeight: FontWeight.w600),
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(right: 10.0),
                          width: 40,
                          child: MaterialButton(
                            height: 35.0,
                            minWidth: 50.0,
                            color: Theme.of(context).primaryColor,
                            textColor: Colors.white,
                            child: Text("-"),
                            onPressed: () {
                              handleDecreseQuatity(traSuaModel);
                            },
                            splashColor: Colors.redAccent,
                          ),
                        ),
                        Container(
                          height: 35,
                          width: 100,
                          child: TextField(
                              controller: TextEditingController(
                                  text: traSuaModel.getQuantity().toString()),
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                              ),
                              keyboardType: TextInputType.number,
                              onChanged: (number) {
                                print(number);
                              }),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10.0),
                          width: 40,
                          child: MaterialButton(
                            height: 35.0,
                            minWidth: 50.0,
                            color: Theme.of(context).primaryColor,
                            textColor: Colors.white,
                            child: Text("+"),
                            onPressed: () {
                              handleIncreseQuatity(traSuaModel);
                            },
                            splashColor: Colors.redAccent,
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}


