import 'package:flutter/material.dart';
import 'model/bill.dart';
import 'model/trasua.dart';

class DetailBill extends StatelessWidget {
  final BillModel bill;
  DetailBill({Key key, this.bill}) : super(key: key);

  _getTotalMoney() {
    var totalMoney = 0;
    for (var ob in bill.listTraSua) {
      totalMoney += ob.price * ob.quantity;
    }
    return totalMoney;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text('Chi tiết đơn hàng'),
      ),
      body: Container(
        margin: EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[Text('Mã Hóa đơn: '), Text(bill.codeBill)],
            ),
            Row(
              children: <Widget>[Text('Trạng thái: '), Text('Inprogess')],
            ),
            Row(
              children: <Widget>[Text('Tên:'), Text(bill.user.fullname)],
            ),
            Row(
              children: <Widget>[Text('Phone:'), Text(bill.user.phone)],
            ),
            Row(
              children: <Widget>[Text('Email:'), Text(bill.user.email)],
            ),
            Row(
              children: <Widget>[Text('Address:'), Text(bill.user.address)],
            ),
            Row(
              children: <Widget>[
                Text('Tổng tiền:'),
                Text('${_getTotalMoney()}\$')
              ],
            ),
            Expanded(
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: ListView(
                      children: bill.listTraSua.map((TraSuaModel traSuaModel) {
                        return _ProductItem(
                          traSuaModel: traSuaModel,
                        );
                      }).toList(),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _ProductItem extends StatelessWidget {
  final TraSuaModel traSuaModel;
  // final int index;

  _ProductItem({
    TraSuaModel traSuaModel,
  })  : traSuaModel = traSuaModel,
        super(key: ObjectKey(traSuaModel));

  @override
  Widget build(BuildContext context) {
    // print(traSuaModel.map);
    // TODO: implement build
    return Container(
      margin: EdgeInsets.only(top: 6, bottom: 6),
      decoration: BoxDecoration(
        border: Border(
          // width: 1,
          // color: Colors.grey[300],
          left: BorderSide(
            //                   <--- left side
            color: Colors.blue[100],
            width: 6.0,
          ),
          top: BorderSide(
            //                    <--- top side
            color: Colors.blue[300],
            width: 4.0,
          ),
          right: BorderSide(
            color: Colors.blue[500],
            width: 6.0,
          ),
          bottom: BorderSide(
            color: Colors.blue[800],
            width: 4.0,
          ),
        ),
      ),
      child: Row(
        children: <Widget>[
          Image(
            image: AssetImage(traSuaModel.asset),
            width: 100,
            height: 80,
            fit: BoxFit.cover,
          ),
          Expanded(
            child: Container(
              margin: EdgeInsets.only(
                  left: 20.0, top: 10.0, right: 10.0, bottom: 10.0),
              // color: Colors.grey,
              child: Column(
                children: <Widget>[
                  Container(
                    alignment: Alignment.centerLeft,
                    child: Text(traSuaModel.name,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 16,
                            color: Colors.lime)),
                  ),
                  Container(
                    alignment: Alignment.centerLeft,
                    margin: EdgeInsets.only(top: 8.0, bottom: 8.0),
                    child: Text(
                      '${traSuaModel.price}\$',
                      style: TextStyle(
                          color: Colors.pink, fontWeight: FontWeight.w600),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Text('So luong: '),
                      Container(
                        child: Text(traSuaModel.getQuantity().toString()),
                      ),
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
