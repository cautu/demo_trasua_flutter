import 'package:flutter/material.dart';
import './model/trasua.dart';
import './cart.dart';

class TraSuaList extends StatefulWidget {
  List<TraSuaModel> listTraSua = getListTraSua();

  static List<TraSuaModel> getListTraSua() {
    List<TraSuaModel> list = List.generate((20), (i) {
      return new TraSuaModel(
        name: 'Trà sữa ${i + 1}',
        price: 10 + i,
        asset: 'images/trasua1.jpeg',
      );
    });
    return list;
  }

  @override
  TraSuaListState createState() {
    // TODO: implement createState
    return TraSuaListState();
  }
}

class TraSuaListState extends State<TraSuaList> {
  Set<TraSuaModel> _cartTraSua = Set<TraSuaModel>();

  void _handeCartTraSuaChanged(TraSuaModel traSuaModel, bool inCart) {
    setState(() {
      if (!inCart) {
        _cartTraSua.add(traSuaModel);
      } else {
        _cartTraSua.remove(traSuaModel);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var _scaffoldKey = new GlobalKey<ScaffoldState>();
    // TODO: implement build
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Center(
          child: Text('Trà sữa'),
        ),
      ),
      body: Container(
          margin: EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Expanded(
                child: GridView.count(
                  crossAxisCount: 2,
                  children: widget.listTraSua.map((TraSuaModel traSuaModel) {
                    return TraSuaItem(
                      traSuaModel: traSuaModel,
                      inCart: _cartTraSua.contains(traSuaModel),
                      onCartChanged: _handeCartTraSuaChanged,
                    );
                  }).toList(),
                ),
              ),
              Container(
                height: 50,
                margin: EdgeInsets.only(left: 10, right: 10),
                padding: EdgeInsets.all(6.0),
                width: double.infinity,
                // decoration: BoxDecoration(
                //   color: Colors.purple,
                // ),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Row(
                      children: <Widget>[
                        Icon(Icons.shopping_cart,
                            color: Colors.deepOrange, size: 30),
                        Text('x '),
                        Text(
                          '${_cartTraSua.length}',
                          style: TextStyle(
                              color: Colors.deepOrange[300],
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                        ),
                      ],
                    )),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.circular(10.0)),
                      padding: EdgeInsets.all(8.0),
                      child: GestureDetector(
                        onTap: () {
                          if (_cartTraSua.length > 0) {
                        
                            for (var ob in _cartTraSua) {
                              ob.setQuantity(1);
                            }
                          
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      Cart(listTraSua: _cartTraSua.toList())),
                            );
                          } else {
                            final snackBar =
                                SnackBar(content: Text('Giỏ hàng trống'));
                            _scaffoldKey.currentState.showSnackBar(snackBar);
                            // Scaffold.of(context).showSnackBar(snackBar);
                          }
                        },
                        // color: Colors.transparent,
                        // textColor: Colors.white,
                        child: Text(
                          'Xem gio hang',
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          )),
    );
  }
}

class TraSuaItem extends StatelessWidget {
  final TraSuaModel traSuaModel;
  final bool inCart;
  final CartChangedCallback onCartChanged;

  TraSuaItem({TraSuaModel traSuaModel, this.inCart, this.onCartChanged})
      : traSuaModel = traSuaModel,
        super(key: ObjectKey(traSuaModel));

  Color _getColor(BuildContext context) {
    return inCart ? Colors.greenAccent : Colors.orange;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: 100,
      margin: EdgeInsets.only(left: 2, right: 2),
      // padding: EdgeInsets.only(right: 8),
      child: Card(
        // semanticContainer: true,
        // margin: EdgeInsets.all(10),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        elevation: 5,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        // color: Colors.purple[50],
        child: Column(
          children: <Widget>[
            InkWell(
              splashColor: Colors.blue.withAlpha(20),
              child: Container(
                // width: 300,
                // height: 200,
                child: Image(
                  image: AssetImage(traSuaModel.asset),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10, left: 10),
              alignment: Alignment.centerLeft,
              child: Text(
                traSuaModel.name,
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.lightGreen,
                    fontSize: 14),
              ),
            ),
            Container(
              margin: const EdgeInsets.only(left: 10, right: 10),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: Text(
                      '${traSuaModel.price}\$',
                      style: TextStyle(color: Colors.orange),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      onCartChanged(traSuaModel, inCart);
                    },
                    child: Icon(
                      Icons.add_shopping_cart,
                      color: _getColor(context),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
