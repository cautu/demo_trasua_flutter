import 'package:flutter/material.dart';
// import 'package:validate/validate.dart';
import 'package:validators/validators.dart' as validator;

import 'model/user.dart';
import 'model/trasua.dart';
import 'model/bill.dart';

import './detailBill.dart';

// class CustomDialog extends StatefulWidget {
//   @override
//   CustomDialogState createState() {
//     // TODO: implement createState
//     return CustomDialogState();
//   }
// }

class CustomDialog extends StatelessWidget {
  final List<TraSuaModel> listTraSuaOrder;
  CustomDialog({Key key, this.listTraSuaOrder}) : super(key: key);

  UserModel userModel = new UserModel();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  TextStyle _getColorText() {
    return TextStyle(color: Colors.purple);
  }

  String _validateEmail(String value) {
    // If empty value, the isEmail function throw a error.
    // So I changed this function with try and catch.
    try {
      validator.isEmail(value);
    } catch (e) {
      return 'The E-mail Address must be a valid email address.';
    }

    return null;
  }

  dialogContent(BuildContext context) {
    return ListView(
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(16.0),
          margin: EdgeInsets.only(top: 16),
          decoration: new BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(16),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min, // To make the card compact
              children: <Widget>[
                Text(
                  'Nhập thông tin của bạn',
                  style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.w700,
                      color: Colors.purple),
                ),
                SizedBox(height: 26.0),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text(
                        'Họ & Tên: ',
                        style: _getColorText(),
                      ),
                    ),
                    Container(
                      height: 35,
                      width: 200,
                      margin: EdgeInsets.only(top: 10.0, bottom: 15.0),
                      child: TextFormField(
                          // controller: TextEditingController(
                          //     text: traSuaModel.getQuantity().toString()),
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                          ),
                          keyboardType: TextInputType.text,
                          onChanged: (value) {
                            userModel.fullname = value;
                          }),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text('Phone: ', style: _getColorText()),
                    ),
                    Container(
                      height: 35,
                      width: 200,
                      margin: EdgeInsets.only(top: 10.0, bottom: 15.0),
                      child: TextField(
                          // controller: TextEditingController(
                          //     text: traSuaModel.getQuantity().toString()),
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                          ),
                          keyboardType: TextInputType.number,
                          onChanged: (value) {
                            userModel.phone = value;
                          }),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text('Email: ', style: _getColorText()),
                    ),
                    Container(
                      height: 35,
                      width: 200,
                      margin: EdgeInsets.only(top: 10.0, bottom: 15.0),
                      child: TextFormField(
                          // controller: TextEditingController(
                          //     text: traSuaModel.getQuantity().toString()),
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                          ),
                          keyboardType: TextInputType.emailAddress,
                          // validator: (value) {
                          //   if (value.isEmpty) {
                          //     return 'Field is required.';
                          //   }
                          //   // _formKey.currentState.save();
                          //   return null;
                          // },
                          // onSaved: (String value) {
                          //   print('onsave $value');
                          // },
                          onChanged: (value) {
                            userModel.email = value;
                          }),
                    )
                  ],
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Text('Address: ', style: _getColorText()),
                    ),
                    Container(
                      // height: 35,
                      width: 200,
                      margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
                      child: TextField(
                          // controller: TextEditingController(
                          //     text: traSuaModel.getQuantity().toString()),
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                          ),
                          maxLines: 3,
                          keyboardType: TextInputType.text,
                          onChanged: (value) {
                            userModel.address = value;
                          }),
                    )
                  ],
                ),
                SizedBox(height: 24.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FlatButton(
                      color: Colors.red,
                      onPressed: () {
                        Navigator.of(context).pop(); // To close the dialog
                      },
                      child: Text(
                        'Hủy'.toUpperCase(),
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    FlatButton(
                      color: Colors.green,
                      onPressed: () {
                        // if (_formKey.currentState.validate()) {
                        // _formKey.currentState.save();

                        final BillModel bill = new BillModel(
                            codeBill: 'BILL0001',
                            listTraSua: listTraSuaOrder,
                            user: userModel);
                        Navigator.of(context).pop();
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => DetailBill(bill: bill),
                            ));
                        // }
                        // To close the dialog
                      },
                      child: Text('Tạo đơn'.toUpperCase(),
                          style: TextStyle(color: Colors.white)),
                    ),
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
